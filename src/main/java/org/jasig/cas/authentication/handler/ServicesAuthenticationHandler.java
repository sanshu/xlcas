package org.jasig.cas.authentication.handler;

import javax.sql.DataSource;
import javax.validation.constraints.NotNull;

import org.jasig.cas.authentication.handler.support.AbstractUsernamePasswordAuthenticationHandler;
import org.jasig.cas.authentication.principal.Credentials;
import org.jasig.cas.authentication.principal.UsernamePasswordCredentials;
import org.jasig.cas.authentication.principal.TppCredentials;
import org.jasig.cas.web.support.WebUtils;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * 
 * 基于用户模块认证接口进行登录认证
 * 
 * @author Administrator
 * 
 */
public class ServicesAuthenticationHandler implements AuthenticationHandler {
	
	private static final Class<TppCredentials> DEFAULT_CLASS = TppCredentials.class;  
	  
    private PasswordEncoder passwordEncoder = new PlainTextPasswordEncoder();  
    private JdbcTemplate jdbcTemplate;  
  
    private DataSource dataSource;  
  
    private PrincipalNameTransformer principalNameTransformer = new NoOpPrincipalNameTransformer();  
    /** Class that this instance will support. */  
    private Class<?> classToSupport = DEFAULT_CLASS;  
    private boolean supportSubClasses = true;  
    @Override  
    public boolean authenticate(Credentials credentials) throws AuthenticationException {  
        final TppCredentials nc = (TppCredentials) credentials;  
        final String username = getPrincipalNameTransformer().transform(nc.getUsername());  
        final String password = nc.getPassword();  
        final String encryptedPassword = this.getPasswordEncoder().encode(  
                password);  
        
   
            try {  
                /*final String sql = "select password from t_admin_user where login_name=?";  
                final String dbPassword = getJdbcTemplate().queryForObject(  
                        sql  
                        , String.class, username);  
                return dbPassword.equals(encryptedPassword);  */
            	if(username.equals(password)) {
            		return true;
            	}
            } catch (final IncorrectResultSizeDataAccessException e) {  
                // this means the username was not found.  
                return false;  
            }  
      
  
        
        return false;  //To change body of implemented methods use File | Settings | File Templates.  
    }  
  
    @Override  
    public boolean supports(Credentials credentials) {  
        return credentials != null  
                && (this.classToSupport.equals(credentials.getClass()) || (this.classToSupport  
                .isAssignableFrom(credentials.getClass()))  
                && this.supportSubClasses);  
    }  
  
    public PasswordEncoder getPasswordEncoder() {  
        return passwordEncoder;  
    }  
  
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {  
        this.passwordEncoder = passwordEncoder;  
    }  
  
    public PrincipalNameTransformer getPrincipalNameTransformer() {  
        return principalNameTransformer;  
    }  
  
    public void setPrincipalNameTransformer(PrincipalNameTransformer principalNameTransformer) {  
        this.principalNameTransformer = principalNameTransformer;  
    }  
  
    public final void setDataSource(final DataSource dataSource) {  
        this.jdbcTemplate = new JdbcTemplate(dataSource);  
        this.dataSource = dataSource;  
    }  
  
    /** 
     * Method to return the jdbcTemplate 
     * 
     * @return a fully created JdbcTemplate. 
     */  
    protected final JdbcTemplate getJdbcTemplate() {  
        return this.jdbcTemplate;  
    }  
  
    protected final DataSource getDataSource() {  
        return this.dataSource;  
    }  

}
