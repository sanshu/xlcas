package org.jasig.cas.authentication.handler;

import java.security.MessageDigest;

public class MyPasswordEncoder implements PasswordEncoder {

	@Override
	public String encode(String message) {
		String encode = "";
		try {
			encode = this.encrypt(message, "MD5");
			encode = this.encrypt(encode.substring(0, 20),  "MD5");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return encode;
	}
	
	public String encrypt(String message, String algorithm)
			throws Exception {
		if (message == null) {
			throw new Exception("message is null.");
		}
		if (!"MD5".equals(algorithm) && !"SHA-1".equals(algorithm)) {
			throw new Exception("algorithm must be MD5 or SHA-1.");
		}
		byte[] buffer = message.getBytes();

		// The SHA algorithm results in a 20-byte digest, while MD5 is 16 bytes
		// long.
		MessageDigest md = MessageDigest.getInstance(algorithm);

		// Ensure the digest's buffer is empty. This isn't necessary the first
		// time used.
		// However, it is good practice to always empty the buffer out in case
		// you later reuse it.
		md.reset();

		// Fill the digest's buffer with data to compute a message digest from.
		md.update(buffer);

		// Generate the digest. This does any necessary padding required by the
		// algorithm.
		byte[] digest = md.digest();

		// Save or print digest bytes. Integer.toHexString() doesn't print
		// leading zeros.
		StringBuffer hexString = new StringBuffer();
		String sHexBit = null;
		for (int i = 0; i < digest.length; i++) {
			sHexBit = Integer.toHexString(0xFF & digest[i]);
			if (sHexBit.length() == 1) {
				sHexBit = "0" + sHexBit;
			}
			hexString.append(sHexBit);
		}
		return hexString.toString();
	}
	
	public static void main(String[] args) {
		MyPasswordEncoder p = new MyPasswordEncoder();
		System.out.println(p.encode("password"));
	}

}
