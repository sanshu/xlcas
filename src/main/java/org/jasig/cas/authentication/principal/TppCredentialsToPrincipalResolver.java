package org.jasig.cas.authentication.principal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class TppCredentialsToPrincipalResolver extends
AbstractPersonDirectoryCredentialsToPrincipalResolver {

	/** Logging instance. */
	private final Log log = LogFactory.getLog(getClass());

/*	public Principal resolvePrincipal(final Credentials credentials) {
		final TppCredentials ciatCredentials = (TppCredentials) credentials;

		if (log.isDebugEnabled()) {
			log.debug("Creating SimplePrincipal for ["
					+ ciatCredentials.getUsername() + "]");
		}

		return new SimplePrincipal(ciatCredentials.getUsername());
	}*/

	public boolean supports(final Credentials credentials) {
		return credentials != null
				&& TppCredentials.class
						.isAssignableFrom(credentials.getClass());
	}

	@Override
	protected String extractPrincipalId(Credentials credentials) {
		final TppCredentials tc = (TppCredentials) credentials;
        return tc.getUsername();
	}
}
